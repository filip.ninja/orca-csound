### Orca & Csound

Goal of this project is to provide Csound library for livecoding with Orca and Vim.
Use:
- Orca as sequencer, event generator 
- Csound in Vim for audio programming

1.Install prerequisites
- Csound (https://csound.com/)
- Orca (https://hundredrabbits.itch.io/orca) or Orca-C (https://github.com/hundredrabbits/Orca-c)
- Csound Vim plugin (https://github.com/luisjure/csound-vim)
- Csound livecoding plugin (https://github.com/kunstmusik/csound-repl)

2. Run Csound
```
csound orca.csd
```

3. Run Orca
4. Livecode orchestra in vim
```
vim session.orc
```

