
massign 1,1
massign 2,2

instr 1
  iFreq = p4
  iAmp = p5
  iAtt = 0.1
  iDec = 0.4
  iSus = 0.6
  iRel = 0.7
  iCutoff = 5000
  iRes = .4
  kEnv = madsr(iAtt, iDec, iSus, iRel) 
  aVco = vco2(iAmp, iFreq)
  aLp = moogladder(aVco, iCutoff*kEnv, iRes)
  aOut =  aLp*kEnv
  outs(aOut, aOut)
endin

gisine = ftgen(0, 0, 1024, 10, 1) 
gicos = ftgen(0, 0, 65536, 9, 1, 1, 90)

opcode xchan, i, Si
  SchanName, initVal xin
   
  Sinit = sprintf("%s_initialized", SchanName)
  if(chnget:i(Sinit) == 0) then
    chnset(1, Sinit)
    chnset(initVal, SchanName)
  endif
  xout chnget:i(SchanName)
endop

opcode xchan, k, Si
  SchanName, initVal xin
    
  Sinit = sprintf("%s_initialized", SchanName)
  if(chnget:i(SchanName) == 0) then
    chnset(1, Sinit)
    chnset(initVal, SchanName)
  endif
  xout chnget:k(SchanName)
endop

instr 2 
  inum = notnum()
  idb = veloc(-30, 0)
  iinstr = wrap(inum, 0, 16)
  printf("p4=%f p5=%f inum=%f idb=%f\n", 1, p4, p5, inum, idb)
  event_i("i", 101, 0, 0.001, ampdbfs(idb))
endin

instr 101 ;BASS DRUM
  xtratim(0.1)
  idur = xchan("BD.decay", 0.5)
  ilevel = xchan("BD.level", 1)
  itune = xchan("BD.tune", 0)
  ipan = xchan("BD.pan", 0.5)
  p3 = 2*i(idur)

  ;SUSTAIN AND BODY OF THE SOUND
  kmul = transeg(0.2, p3*0.5, -15, 0.01, p3*0.5, 0, 0)
  kbend = transeg(0.5, 1.2, -4, 0, 1, 0, 0)
  asig = gbuzz(0.5, 50*octave(itune)*semitone(kbend), 20, 1, kmul, gicos)
  aenv = transeg:a(1, p3-0.004, -6, 0)
  aatt = linseg:a(0, 0.004, 1)
  asig = asig*aenv*aatt

  ;HARD, SHORT ATTACK OF THE SOUND
  aenv = linseg:a(1, 0.07, 0)
  acps = expsega(400, 0.07, 0.001, 1, 0.001)
  aimp = oscili(aenv, acps*octave(itune*0.25), gisine)

  amix = ((asig*0.5)+(aimp*0.35))*ilevel*p4

  aL,aR pan2 amix, ipan 
  outs(aL, aR)
endin

