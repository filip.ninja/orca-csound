<CsoundSynthesizer>
<CsOptions>
;-+rtaudio=jack 
-o dac -+rtmidi=alsaseq -Ma --midi-key-cps=4 --midi-velocity-amp=5 -m0 --port=10000
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	24000
ksmps	=	128
nchnls	=	2
0dbfs	=	1

#include "orca.orc"

</CsInstruments>
; ==============================================
<CsScore>
f0 z
</CsScore>
</CsoundSynthesizer>

